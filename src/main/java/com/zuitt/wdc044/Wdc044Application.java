package com.zuitt.wdc044;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
//Annotation  - under the hood codes to be used.
@RestController
// handles the endpoint for web requests

public class Wdc044Application {
	public static void main(String[] args) {
		SpringApplication.run(Wdc044Application.class, args);
	}
	// @RequestParam - annotation that tells spring to expect a name value in the request

	@GetMapping("/hello")
	// @GetMapping - tells spring to use this method when a GET request is received
	public String hello(@RequestParam(value="name", defaultValue = "World") String name){
		return String.format("Hello %s", name);
		// %s - any type but the return is a String
		// %c - character and the output is a unicode character
		// %b - any type but the output is boolean
	}

	@GetMapping("/hi")
	// @GetMapping - tells spring to use this method when a GET request is received
	public String hi(@RequestParam(value="name", defaultValue = "user") String name){
		return String.format("Hi %s", name);
		// %s - any type but the return is a String
		// %c - character and the output is a unicode character
		// %b - any type but the output is boolean
	}

	@GetMapping("/nameAge")
	// @GetMapping - tells spring to use this method when a GET request is received
	public String namege(@RequestParam(value="age", defaultValue = "") String age){
		return String.format("Hello Juan! Your age is %s.", age);
		// %s - any type but the return is a String
		// %c - character and the output is a unicode character
		// %b - any type but the output is boolean
	}
}
// ./mvnw spring-boot:run
// to run the application
// route for checking: localhost:8080/hello
// with parameter: localhost:8080/hello?parameter=value
// example :   http://localhost:8080/hello?name=tine